# TITLE

## Description

"Pick & Grow" game written in JS, using HTML 5 canvas.

Originally based on the video tutorial "How to Make a Game with JavaScript and HTML Canvas | Keyboard Input & Sprite Animation [Vanilla JS]" from "Franks laboratory" YouTube channel (links below).


## Usage

Enter "index.html" path in browser address bar.


## Links

* Original [video tutorial](https://youtu.be/EYf_JwzwTlQ)
* ["Franks laboratory" YouTube channel](https://www.youtube.com/@Frankslaboratory)
